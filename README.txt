
-- SUMMARY --

sIFR is a web technology that uses Flash and javascript to render html text 
using font files provided by the web server. It is totally standards compliant
and provides great advantages over the text-inside-a-graphic design "tricks" of 
the past. This module provides a very easy, user-friendly interface for 
implementing sIFR quickly on any Drupal site without requiring any adjustments 
to the site theme.

For a full description visit the project page:
  http://drupal.org/project/sifr
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/sifr


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Copy sifr module to your modules directory and enable it on the admin
  modules page.

* Create a sub-folder "sifr" in the module folder of sifr, i.e. modules/sifr/sifr.

* Download sifr from http://novemberborn.net/sifr

* Copy the following files of that archive into modules/sifr/sifr:

  - sifr.js
  - sifr-addons.js
  - sIFR-print.css
  - sIFR-screen.css


-- CONFIGURATION --

* Upload your .swf font files at admin/settings/sifr/manage

* Create replacement rules to apply for each font file at 
  admin/settings/sifr/addrule


-- TROUBLESHOOTING --

* Always choose CSS selectors as specific as possible. Instead of just

  'li a' 
  use
  '#header ul.primary-links a' (or similar)

  for your rules.

* Rule weight is important. A text element in your website is replaced only
  once. If you want to replace certain elements that are already covered by
  another rule, the rule replacing those certain elements has to come first.

* If you experience weird font sizings and other presentation 'bugs' try
  altering your stylesheets and sifr settings for

  font-size, padding, margin, line-height

* If text elements are prepended with sifr fonts but not replaced, check if your
  stylesheets were properly loaded (by observing HTML Header) and if you are
  overwriting 'visibility' somewhere in your theme.

* Further help and support for sIFR JavaScript may be found at
  - http://novemberborn.net/sifr
  - http://www.mikeindustries.com/sifr
  - http://wiki.novemberborn.net/sifr/
  - http://forum.textdrive.com/viewforum.php?id=20


-- NOTES --

* Please read the (very easy) sIFR documentation included in the download
  package about creating your own custom sIFR fonts (requires Macromedia/Adobe
  Flash).

* Remember to check copyright / font licenses in front of publishing a font in
  the web.


-- FAQ --

Q: Why do sIFR links open in a new window instead of the current (_self)?

A: This seems to be a bug in the sIFR Action Script. If you still experience
   this bug and you are using the latest sIFR version available on
   http://www.mikeindustries.com/sifr/, you need to
   - alter the file dont_customize_me.as as described here
   - and re-create your sIFR fonts.

   In line 26 change:
<code>
function launchURL (index) {
	launchURL_anchor = eval("sifr_url_"+index);
	launchURL_target = eval("sifr_url_"+index+"_target");
	getURL(launchURL_anchor,launchURL_target);
}
</code>

   To:
<code>
function launchURL (index) {
	launchURL_anchor = eval("sifr_url_"+index);
	launchURL_target = eval("sifr_url_"+index+"_target");
	if ( launchURL_target != undefined ) {
		getURL(launchURL_anchor,launchURL_target);
	}
	else {
		getURL(launchURL_anchor);
	}
}
</code>


-- CONTACT --

Current maintainers:
* Daniel F. Kudwien (sun) - dev@unleashedmind.com

Previous maintainers:
* Jeff Robbins (jjeff) - www.jjeff.com

This project has been sponsored by:
* UNLEASHED MIND
  Specialized in consulting and planning of Drupal powered sites, UNLEASHED
  MIND offers installation, development, theming, customization, and hosting
  to get you started. Visit http://www.unleashedmind.com for more information.

* Bryght
  Visit http://www.bryght.com for more information.

